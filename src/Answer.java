import java.util.*;

public class Answer {

   public static void main (String[] param) {

      // TODO!!! Solutions to small problems 
      //   that do not need an independent method!
    
      // conversion double -> String
	  String s1 = String.valueOf(Math.PI);
	  String s2 = Double.toString(Math.E);
	  System.out.println("Conversion double -> String: PI = \"" + s1 + "\"");
	  System.out.println("                              E = \"" + s2 + "\"");

      // conversion String -> int
	  String s3 = "100";
	  try {
		  int i1 = Integer.parseInt(s3);
		  System.out.println("Conversion String -> int: \"" + s3 + "\" = " + i1);
	  } catch (NumberFormatException nfe) {
		  System.err.println("Argument \"" + s3 + "\" is not an integer.");
	  }

      // "hh:mm:ss"
	  Calendar cal = Calendar.getInstance();
	  String currentTime = cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND);
	  System.out.println("Current time: " + currentTime);

      // cos 45 deg
	  double angDeg = 45;
	  double cos = Math.cos(Math.toRadians(angDeg));
	  System.out.println("Cos " + angDeg + " degrees: " + cos);

      // table of square roots
	  System.out.println("Table of square roots:");
	  String divider = "------------------------";
	  System.out.printf("%9s | %s %n", "Argument", "Square root");
	  System.out.println(divider);
	  for (int i = 0; i <= 100; i = i + 5) {
		  System.out.printf("%9d | %f %n", i, Math.sqrt(i));
	  }
	  System.out.println(divider);
	  
      String firstString = "ABcd12";
      String result = reverseCase (firstString);
      System.out.println ("\"" + firstString + "\" -> \"" + result + "\"");

      // reverse string
      String str = "1234ab";
      StringBuffer sb = new StringBuffer();
      for (int i = 0; i < str.length(); i++) {
    	  sb.append(str.charAt(str.length() - 1 - i));
      }
      String rev = sb.toString();
      System.out.println("\"" + str + "\" -> \"" + rev);

      String s = "How  many	 words 	 here";
      int nw = countWords (s);
      System.out.println (s + "\t" + String.valueOf (nw));

      // pause. COMMENT IT OUT BEFORE JUNIT-TESTING!
      long start = System.currentTimeMillis();
      try {
          Thread.sleep(3000);    	  
      } catch(InterruptedException ex) {
    	  System.out.println(ex);;
      }
      long end = System.currentTimeMillis();
      long diff = end - start;
      System.out.println("Difference in milliseconds: " + diff);

      final int LSIZE = 100;
      ArrayList<Integer> randList = new ArrayList<Integer> (LSIZE);
      Random generaator = new Random();
      for (int i=0; i<LSIZE; i++) {
         randList.add (new Integer (generaator.nextInt(1000)));
      }
      System.out.println("List of 100 random integers: " + randList);

      // minimal element
      Integer min = new Integer(randList.get(0));
      for (Integer elem: randList) {
    	 if (min.compareTo(elem) > 0) {
    		 min = elem;
    	 }
      }
      System.out.println("List min: " + min);
      Collections.sort(randList);
      System.out.println("Sorted list (min test): " + randList);

      // HashMap tasks:
      //    create
      HashMap<String, String> subjects = new HashMap<String, String>();
      subjects.put("I231", "Algoritmid ja andmestruktuurid");
      subjects.put("I245", "Andmebaasisüsteemide alused");
      subjects.put("I243", "Programmeerimine C# keeles");
      subjects.put("I397", "Mobiilirakenduste arendamine Android platvormile");
      subjects.put("I112", "Loogika ja algoritmiteooria");
      //    print all keys
      Set<String> keys = subjects.keySet();
      System.out.println("Set of subject codes:");
      for (String elem: keys) {
         System.out.println(elem); 
      }
      //    remove a key
      String subName = subjects.remove("I243");
      System.out.println("Removed subject: \"" + subName + "\"");
      //    print all pairs
      System.out.println("Table of subjects:");
      Iterator<String> it = subjects.keySet().iterator();
      while (it.hasNext()) {
    	  String subCode = it.next();
    	  System.out.println(subCode + " " + subjects.get(subCode));
      }

      System.out.println ("Before reverse:  " + randList);
      reverseList (randList);
      System.out.println ("After reverse: " + randList);

      System.out.println ("Maximum: " + maximum (randList));
   }

   /** Finding the maximal element.
    * @param a Collection of Comparable elements
    * @return maximal element.
    * @throws NoSuchElementException if <code> a </code> is empty.
    */
   static public <T extends Object & Comparable<? super T>>
         T maximum (Collection<? extends T> a) 
            throws NoSuchElementException {
	  if (a.isEmpty()) {
	     throw new NoSuchElementException("Collection is empty.");
	  }
	  Iterator<? extends T> it = a.iterator();
	  T max = it.next();
	  while (it.hasNext()) {
		  T elem = it.next();
		  if (elem.compareTo(max) > 0) {
			  max = elem;
		  }
	  }
      return max;
   }

   /** Counting the number of words. Any number of any kind of
    * whitespace symbols between words is allowed.
    * @param text text
    * @return number of words in the text
    */
   public static int countWords (String text) {
	  StringTokenizer st = new StringTokenizer(text);
      return st.countTokens();
   }

   /** Case-reverse. Upper -> lower AND lower -> upper.
    * @param s string
    * @return processed string
    */
   public static String reverseCase (String s) {
      StringBuffer sb = new StringBuffer();
      for (int i = 0; i < s.length(); i++) {
         char c = s.charAt(i);
         if (Character.isUpperCase(c)) {
            c = Character.toLowerCase(c);
         } else {
            c = Character.toUpperCase(c);
         }
    	 sb.append(c);
      }
      return sb.toString();
   }

   /** List reverse. Do not create a new list.
    * @param list list to reverse
    */
   public static <T extends Object> void reverseList (List<T> list)
      throws UnsupportedOperationException {
	   T[] arr = (T[]) new Object[list.size()];
	   list.toArray(arr);
	   for (int i = 0; i < arr.length; i++) {
		   list.set(i, arr[arr.length - 1 - i]);
	   }
   }
}
